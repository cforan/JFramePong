/**
 *	@author Cathal Foran
 */

public class MathUtils{

	public static float clamp(float value, float min, float max){
		
		return (value < min)? min : (value > max)? max : value;
	}
	
	public static float randomInvert(float value, float chance){
		
		return (Math.random() < chance)? value: -value;
	}
}