/**
 *	This class describes a JFrame window that is able to dynamically move
 *	around the screen based on a speed value
 *	
 *	@author Cathal Foran
 */

import javax.swing.JFrame;
import java.util.ArrayList;
import java.awt.Dimension;

public class MovingFrame extends JFrame{
	/**
	 *	List of all <b>MovingFrame</b>s created
	 */
	public static ArrayList<MovingFrame> frames = new ArrayList<MovingFrame>();
	
	/**
	 *	Distance the MovingFrame will move when update() is called
	 */
	private Vector2 speed;
	
	/**
	 *	Current position of the window
	 */
	private Vector2 position; // store second location variable as float, avoids rounding problems
	
	/**
	 *	Minimum x and y value the window can be located at
	 */
	private Vector2 minPos;
	
	/**
	 *	Maximum x and y value the window can be located at
	 */
	private Vector2 maxPos;
	
	/**
	 *	Creates a new instance of the <b>MovingFrame</b> class.
	 *	@param	title		The title of the window
	 *	@param	position	The position of the window on the current monitor
	 *	@param	size		The size of the window in pixels
	 *	@param	speed		The distance the window will move every time update() is called
	 */
	public MovingFrame(String title, Vector2 position, Vector2 size, Vector2 speed){
		
		super(title);
		this.setAreaBounds(Vector2.MIN, Vector2.MAX);
		this.setSize((int)size.x, (int)size.y);
		this.setPosition(position);
		this.setSize((int)size.x, (int)size.y);
		this.setSpeed(speed);
		this.setVisible(true);
		this.setResizable(false);
		this.addWindowListener(new WindowCloseListener()); // exit program when any MovingFrame is closed
		frames.add(this);
	}
	
	/**
	 *	Creates a new instance of the <b>MovingFrame</b> class. This constructor will position the window to the
	 *	 center of the screen, and set a size of 200 by 200 pixels.
	 *	@param	title	The title of the window
	 */
	public MovingFrame(String title){
		
		this(title, Vector2.ZERO, new Vector2(200, 200), Vector2.ZERO);
	}
	
	/**
	 *	Creates a new instance of the <b>MovingFrame</b> class. This constructor will position the window to the
	 *	 center of the screen, set a size of 200 by 200 pixels, and set a default title of "MovingFrame".
	 */
	public MovingFrame(){
		
		this("MovingFrame");
	}
	
	/**
	 *	Sets the speed vector to a new value.
	 *	@param	speed	The new speed vector for the MovingFrame
	 */
	public void setSpeed(Vector2 speed){
		
		this.speed = speed;
	}
	
	/**
	 *	Gets the speed vector of the MovingFrame as a <b>Vector2</b>.
	 *	@return	Vector2
	 */
	public Vector2 getSpeed(){
		
		return this.speed;
	}
	
	/**
	 *	Increments the position of the MovingFrame by the values stored in <b>speed</b>.
	 */
	public void update(){
		
		this.setPosition(this.position.x + this.speed.x, this.position.y + this.speed.y);
		this.validate();
	}
	/**
	 *	Sets the area that the frame is confined to
	 *	@param	min		Minimum x and y value the window can be located at
	 *	@param	max		Maximum x and y value the window can be located at
	 */
	public void setAreaBounds(Vector2 min, Vector2 max){
		
		this.minPos = min;
		this.maxPos = max;
	}
	
	/**
	 *	Gets the size of the MovingFrame as a Vector2 object
	 *	@return Vector2
	 */
	public Vector2 getVectorSize(){
		
		Dimension d = this.getSize();
		return new Vector2(d.width, d.height);
	}
	
	/**
	 *	Sets the position of the MovingFrame to the given position vector
	 *	@param	position	The position of the MovingFrame
	 */
	public void setPosition(Vector2 position){
		
		this.position = Vector2.clamp(
				position, 
				this.minPos, 
				new Vector2(
						this.maxPos.x - this.getContentPane().getWidth(),
						this.maxPos.y - this.getContentPane().getHeight()
				)
		);
		this.setLocation((int)position.x, (int)position.y);
	}
	
	/**
	 *	Sets the position of the MovingFrame to the vector defined by the given X and Y values
	 *	@param	x	The X value for the position of the MovingFrame
	 *	@param	y	The Y value for the position of the MovingFrame
	 */
	public void setPosition(float x, float y){
		
		setPosition(new Vector2(x, y));
	}
	
	/**
	 *	Gets the position vector of the MovingFrame as a <b>Vector2</b>.
	 *	@return	Vector2
	 */
	public Vector2 getPosition(){
		
		return this.position;
	}
}
