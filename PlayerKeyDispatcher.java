/**
 *	Dispatches key events to each player instance
 *	@author Cathal Foran
 */

import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;

public class PlayerKeyDispatcher implements KeyEventDispatcher{
		
		//using KeyEventDispatcher to allow listening for key presses independently of focused window
		@Override
		public boolean dispatchKeyEvent(KeyEvent event){
			
			switch(event.getID()){
				
				case KeyEvent.KEY_PRESSED:
					for(Player p: GameController.getPlayerList()){
						p.keyPressed(event);
					}
					
					if((char)event.getKeyCode() == ' ') GameController.togglePause(); //pauses game if spacebar is pressed
					
					break;
				case KeyEvent.KEY_RELEASED:
					for(Player p: GameController.getPlayerList()){
						p.keyReleased(event);
					}
					break;
				case KeyEvent.KEY_TYPED:
					for(Player p: GameController.getPlayerList()){
						p.keyTyped(event);
					}
					break;
			}
			return false;
		}
	}