/**
 *	Describes a vector with two components X and Y
 *	
 *	@author Cathal Foran
 */

public class Vector2{
	
	public float x, y;
	
	public static final Vector2 ZERO = new Vector2(0, 0);
	public static final Vector2 MIN = new Vector2(Integer.MIN_VALUE, Integer.MIN_VALUE);
	public static final Vector2 MAX = new Vector2(Integer.MAX_VALUE, Integer.MAX_VALUE);
	
	public Vector2(float x, float y){
		
		this.x = x; 
		this.y = y;
	}
	
	public Vector2(){
		
		this(0, 0);
	}
	
	public static float dist(Vector2 v1, Vector2 v2){
		
		return (float)Math.sqrt(Math.pow(v2.x - v1.x, 2f) + Math.pow(v2.y - v1.y, 2f));
	}
	
	public static Vector2 add(Vector2 v1, Vector2 v2){
		
		return new Vector2(v1.x + v2.x, v1.y + v2.y);
	}
	
	public static Vector2 multiply(float f, Vector2 v){
		
		return new Vector2(v.x * f, v.y * f);
	}
	
	public float dist(Vector2 v){
		
		return dist(this, v);
	}
	
	public static Vector2 clamp(Vector2 v, Vector2 min, Vector2 max){
		
		Vector2 result = new Vector2(
				MathUtils.clamp(v.x, min.x, max.x),
				MathUtils.clamp(v.y, min.y, max.y)
		);
		return result;
	}
	
	public String toString(){
		
		return String.format("[%.2f, %.2f]", this.x, this.y);
	}
}