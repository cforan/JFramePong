/**
 *	Handles saving and loading the settings file
 *	@author Cathal Foran
 */

import java.io.File;
import java.nio.file.Path;
import java.net.URI;
import java.io.*;
import javax.swing.JOptionPane;

public class FileSaveHandler{
	
	public static final String SAVE_FILE_LOCATION = "file:/" + System.getProperty("user.dir").replace("\\", "/") + "/saveFile.dat";
		
	public static void save(Settings s){
		
		try{
			
			File saveFile = new File(new URI(SAVE_FILE_LOCATION));
			
			if(!saveFile.exists()){
				saveFile.createNewFile();
			}
			
			ObjectOutputStream output = getObjOutStream(saveFile);
			
			output.writeObject(s);
		}catch(Exception e){
			
			showErrorDialog("Error saving file");
			e.printStackTrace();
		}
	}
	
	public static Settings load(){
		
		Settings s = null;
		try{
			File saveFile = new File(new URI(SAVE_FILE_LOCATION));
			if(saveFile.exists()){
				ObjectInputStream input = getObjInStream(saveFile);
				s = (Settings) input.readObject();
			}
		} catch (Exception e){
			
			showErrorDialog("Error loading save file");
			e.printStackTrace();
		}
		
		s = (s == null)? new Settings(): s;
		
		return s;
	}
	
	private static void showErrorDialog(String message){
		
		JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	private static ObjectOutputStream getObjOutStream(File file){
		
		ObjectOutputStream o = null;
		
		try{
			o = new ObjectOutputStream(new FileOutputStream(file));
		} catch (Exception e){}
		return o;
	}
	
	private static ObjectInputStream getObjInStream(File file){
		
		ObjectInputStream o = null;
		
		try{
			o = new ObjectInputStream(new FileInputStream(file));
		} catch (Exception e){}
		return o;
	}
}