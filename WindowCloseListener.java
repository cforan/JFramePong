/**
 *	Listens for window close event and exits the program
 *	
 *	@author Cathal Foran
 */

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class WindowCloseListener extends WindowAdapter{
	
	public void windowClosing(WindowEvent e){
		
		FileSaveHandler.save(GameController.getSettings());
		System.exit(0);
	}
}