/**
 *	Test class for MovingFrame, creates a window at center of screen with random speed
 *
 *	@author Cathal Foran
 */

import java.awt.Toolkit;

public class MovingFrameTest{
	
	private static int width;
	private static int height;
	
	private static MovingFrame f;
	private static boolean shouldExit;
	
	public static void main(String[] args){
		
		shouldExit = false;
		Toolkit tk = Toolkit.getDefaultToolkit();
		
		width = (int)tk.getScreenSize().getWidth();
		height = (int)tk.getScreenSize().getHeight();
		
		f = new MovingFrame(
				"frame 1", 
				new Vector2(width / 2, height / 2), 
				new Vector2(200, 200), 
				new Vector2(((float)Math.random() * 10f) - 5f, ((float)Math.random() * 10f) - 5f)
		);
		
		loop();
	}
	
	private static void loop(){
		
		while(true){
			
			for(MovingFrame f : MovingFrame.frames){
					
				f.update();
				//System.out.println(f.getSpeed());
			}
			
			try{
				Thread.sleep((long)(1000f/60f));
			} catch(Exception e){}
		}
	}
}