/**
 *	Manages persistent data
 *	@author Cathal Foran
 */

import java.io.Serializable;

public class Settings implements Serializable{
	
	public int player1Color;
	public int player2Color;
	public float gameSpeed;
	
	public Settings(){
		
		this.player1Color = 0xFF0000;
		this.player2Color = 0x00FFFF;
		this.gameSpeed = 15;
	}
	
	public String toString(){
		
		return String.format("P1Color: %d\nP2Color: %d\nGameSpeed: %.2f\n", player1Color, player2Color, gameSpeed);
	}
}