/**
 *	Entry point for program, manages game objects, main game loop, and key distpatch events
 *	@author Cathal Foran
 */

import java.awt.Toolkit;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.KeyboardFocusManager;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Color;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;


public class GameController extends MovingFrame implements ActionListener{
	
	private static boolean isPaused;
	private static int width, height;
	private static Player p1, p2;
	private static Settings gameSettings;
	private static GameController gameController;
	private static ArrayList<Player> players = new ArrayList<Player>();
	private JLabel pauseLabel;
	
	public GameController(){
		
		super("gamecontroller");
		
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new PlayerKeyDispatcher());
		this.getContentPane().setLayout(new FlowLayout());
		this.setPosition(new Vector2((width - this.getSize().width) / 2, (height - this.getSize().height)/2));
		this.setJMenuBar(makeMenuBar());
		this.pauseLabel = new JLabel("<html><span style=\"font-size:20px; color:#000;\">PAUSED</span></hmtl>");
		this.pauseLabel.setVisible(false);
		this.add(pauseLabel);
		validate();
	}
	
	public static void main(String[] args){
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		
		gameSettings = FileSaveHandler.load();
		width = (int)tk.getScreenSize().getWidth();
		height = (int)tk.getScreenSize().getHeight();
	
		gameController = new GameController();
		
		p1 = new Player(Vector2.ZERO, 'Q', 'A', new Color(gameSettings.player1Color));
		p2 = new Player(new Vector2(width - Player.WIDTH, 0), 'O', 'L', new Color(gameSettings.player2Color));
		
		players.add(p1);
		players.add(p2);
		
		Collider.addCollisionPair(p1, gameController);
		Collider.addCollisionPair(p2, gameController);
		
		for(Player p : players){
			p.setAreaBounds(Vector2.ZERO, new Vector2(width, height));
		}
		loop();
	}

	public void actionPerformed(ActionEvent event){
		
		switch(event.getActionCommand()){
			
			case "Restart Game":
				restartGame();
				break;
			case "Game Settings...":
				showGameSettings();
				break;
		}
	}
	
	public static void togglePause(boolean shouldPause){
		
		if(shouldPause && !isPaused || !shouldPause && isPaused) togglePause();
	}
	
	public static void togglePause(){
		
		isPaused = (isPaused)? false : true;
		gameController.pauseLabel.setVisible(isPaused);
		gameController.validate();
	}
	
	private Vector2 getRandomSpeed(){
		
		return new Vector2(	MathUtils.randomInvert(gameSettings.gameSpeed, 0.5F),
							MathUtils.randomInvert(gameSettings.gameSpeed, 0.5F)
		);
	}
	
	private void showGameSettings(){
		
		togglePause(true);
		
		JPanel settingsPanel = new JPanel(new GridLayout(6, 1));
		
		int color1 = gameSettings.player1Color;
		int color2 = gameSettings.player2Color;
		float speed = gameSettings.gameSpeed;
		
		JLabel p1ColorLabel = new JLabel("Player 1 color:");
		JTextField p1ColorField = new JTextField(String.format("%06X", color1));
		p1ColorLabel.setLabelFor(p1ColorField);
		
		JLabel p2ColorLabel = new JLabel("Player 2 color:");
		JTextField p2ColorField = new JTextField(String.format("%06X", color2));
		p2ColorLabel.setLabelFor(p2ColorField);
		
		JLabel gameSpeedLabel = new JLabel("Game speed:");
		JTextField gameSpeed = new JTextField(String.format("%.1f", speed));
		gameSpeedLabel.setLabelFor(gameSpeed);
				
		settingsPanel.add(p1ColorLabel);
		settingsPanel.add(p1ColorField);
		settingsPanel.add(p2ColorLabel);
		settingsPanel.add(p2ColorField);
		settingsPanel.add(gameSpeedLabel);
		settingsPanel.add(gameSpeed);
		settingsPanel.validate();
				
		int settingsResult = JOptionPane.showConfirmDialog(null, settingsPanel, "Settings", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		
		if(settingsResult == JOptionPane.OK_OPTION){
			
			NumberFormatException event = null;
			
			try{
				if(p1ColorField.getText().length() < 6 || p2ColorField.getText().length() < 6){
					throw new NumberFormatException("Not enough digits specified");
				}
				color1 = Integer.parseInt(p1ColorField.getText(), 16);
				color2 = Integer.parseInt(p2ColorField.getText(), 16);
				speed = Float.parseFloat(gameSpeed.getText());
				
				gameSettings.player1Color = color1;
				gameSettings.player2Color = color2;
				gameSettings.gameSpeed = speed;
			} catch (NumberFormatException e){
				
				event = e;
				JOptionPane.showMessageDialog(null, "Invalid values", "Error", JOptionPane.ERROR_MESSAGE);
			}
			
			if(event == null){
				
				JOptionPane.showMessageDialog(null, "Restart game to see changes", "Settings Changed", JOptionPane.INFORMATION_MESSAGE);
			}
		}		
	}
	
	private void restartGame(){
		
		p1.resetScore();
		p2.resetScore();
		resetGameController();
	}
	
	private void resetGameController(){
		
		setPosition(new Vector2((width - getVectorSize().x) / 2, (height - getVectorSize().y)/2)); //center of screen
		setSpeed(getRandomSpeed());
		togglePause(true);
	}
	
	private JMenuBar makeMenuBar(){
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu game = new JMenu("Game");
		
		JMenuItem restartGameItem = new JMenuItem("Restart Game");
		restartGameItem.addActionListener(this);
		JMenuItem gameSettingsItem = new JMenuItem("Game Settings...");
		gameSettingsItem.addActionListener(this);
		
		game.add(restartGameItem);		
		game.addSeparator();
		game.add(gameSettingsItem);
		
		menuBar.add(game);

		return menuBar;
	}
	
	private static void checkScoreCount(){
		
		if(gameController.getPosition().x < 0){
			
			p2.incrementScore();
			gameController.resetGameController();
		} else if (Vector2.add(gameController.getPosition(), gameController.getVectorSize()).x > width){
			
			p1.incrementScore();
			gameController.resetGameController();
		}
	}
	
	public static void checkPlayerWin(Player plr){
		
		if(plr.getScore() >= 10){
			
			gameController.togglePause(true);
			
			int plrColor;
			String plrName;
			if(plr == p1){
				
				plrColor = gameSettings.player1Color;
				plrName = "Player 1";
			}else if(plr == p2){
				
				plrColor = gameSettings.player2Color;
				plrName = "Player 2";
			}else {
				
				plrColor = 0;
				plrName = "_";
			}
			JPanel winPanel = new JPanel(new GridLayout(1, 1));
			JLabel playerLabel = new JLabel("<html><b><span style=\"color:" + Integer.toHexString(plrColor) + ";\">" + plrName + "</span></b> wins</html>");
			
			winPanel.add(playerLabel);
			winPanel.validate();
			JOptionPane.showMessageDialog(null, winPanel, "Game Over", JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	public static void loop(){
				
		while(true){
			
			if(!isPaused){
				for(MovingFrame f: MovingFrame.frames){
					f.update();
				}
				Collider.checkCollisions();
				Collider.collisionBoundary(2, gameController, Vector2.ZERO, new Vector2(width, height));
				checkScoreCount();
			}
			
			try{
				Thread.sleep((long)(1000f/60f));
			} catch(Exception e){}
		}
	}
	
	public static GameController getGameController(){ return gameController; }
	public static Settings getSettings(){ return gameController.gameSettings; }
	public static ArrayList<Player> getPlayerList(){ return players; }
}
