/**
 *	Handles the collision between MovingFrame objects, assuming only one of the two objects involved reacts
 *	@author Cathal Foran
 */

import java.util.ArrayList;
 
public class Collider{
	
	private static ArrayList<CollisionPair> colliders = new ArrayList<CollisionPair>();
	
	public static void checkCollisions(){
		
		for(CollisionPair cp : colliders){
			
			if(isColliding(cp) == 1){
				react(1, cp);
			} else if(isColliding(cp) == 2){
				react(2, cp);
			}
		}
	}
	
	//confines MovingFrame objects inside boundary, object will collide with boundary
	public static void collisionBoundary(int axes, MovingFrame obj, Vector2 min, Vector2 max){
		
		if((axes & 1) == 1){
			
			if(obj.getPosition().x + obj.getSpeed().x < min.x || obj.getPosition().x + obj.getVectorSize().x + obj.getSpeed().x > max.x){
				
				reverseSpeed(1, obj);
			}
		}
		if((axes & 2) == 2){
			
			if(obj.getPosition().y + obj.getSpeed().y < min.y || obj.getPosition().y + obj.getVectorSize().y + obj.getSpeed().y > max.y){
				
				reverseSpeed(2, obj);
			}
		}
	}
	
	public static void addCollisionPair(MovingFrame staticObj, MovingFrame dynamicObj){
		
		colliders.add(new Collider().new CollisionPair(staticObj, dynamicObj));
	}
	/*
		0 = not colliding
		1 = collided on x axis
		2 = collided on y axis
	*/
	private static int isColliding(CollisionPair collisionPair){ 
		
		Vector2 staticPos = Vector2.add(collisionPair.staticCollider.getPosition(), collisionPair.staticCollider.getSpeed()); // add speed to position to get position in next frame
		Vector2 staticSize = collisionPair.staticCollider.getVectorSize();
		Vector2 dynamicPos = Vector2.add(collisionPair.dynamicCollider.getPosition(), collisionPair.dynamicCollider.getSpeed());
		Vector2 dynamicSize = collisionPair.dynamicCollider.getVectorSize();
		
		boolean xCollide = false, yCollide = false;
		
		if(dynamicPos.x < staticPos.x + staticSize.x && dynamicPos.x > staticPos.x - dynamicSize.x){ // if x positions overlap
			collisionPair.yOverlap++;
			yCollide = true;
		} else {
			collisionPair.yOverlap = 0;
			yCollide = false;
		}
		
		if(dynamicPos.y < staticPos.y + staticSize.y && dynamicPos.y > staticPos.y - dynamicSize.y){ // if y positions overlap
			collisionPair.xOverlap++;
			xCollide = true;
		} else {
			collisionPair.xOverlap = 0;
			xCollide = false;
		}
		
		int result = (xCollide && yCollide)? (collisionPair.xOverlap > collisionPair.yOverlap)? 1 : 2 : 0; // if(x collides and y collides){ if(x first collision){return 1} else {return 2}} else {return 0}
		
		if(xCollide && yCollide){
			collisionPair.xOverlap = 0;
			collisionPair.yOverlap = 0;
		}
		return result;
	}
	
	private static void react(int axis, CollisionPair cp){
		
		reverseSpeed(axis, cp.dynamicCollider);
	}
	
	private static void reverseSpeed(int axes, MovingFrame mvFrame){
		
		if((axes & 1) == 1){
			
			mvFrame.setSpeed(new Vector2(-mvFrame.getSpeed().x, mvFrame.getSpeed().y));
		}
		if((axes & 2) == 2){
			
			mvFrame.setSpeed(new Vector2(mvFrame.getSpeed().x, -mvFrame.getSpeed().y));
		}
	}
	
	private class CollisionPair{
		
		public MovingFrame staticCollider;
		public MovingFrame dynamicCollider;
		
		public int xOverlap;
		public int yOverlap;
	
		public CollisionPair(MovingFrame staticCollider, MovingFrame dynamicCollider){
			
			this.staticCollider = staticCollider;
			this.dynamicCollider = dynamicCollider;
			this.xOverlap = 0;
			this.yOverlap = 0;
		}
	}
}