/**
 *	controls frames at sides of screen
 *	
 *	@author	Cathal Foran
 */

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JLabel;

public class Player extends MovingFrame implements KeyListener{
	
	private char upKey, downKey;
	private int score;
	private JLabel scoreText;
	
	public static final int WIDTH = 150;
	public static final int HEIGHT = 350;
	
	private static float playerSpeed = GameController.getSettings().gameSpeed;
	
	private static final String SCORE_TEXT_START = "<html><span style=\"font-size:100px;color:#000;\">";
	private static final String SCORE_TEXT_END = "</span></html>";
			
	public Player(String title, Vector2 position, char upKey, char downKey, Color backgroundColor){
		
		super(title, position, new Vector2(WIDTH, HEIGHT), Vector2.ZERO);
		this.getContentPane().setBackground(backgroundColor);
		this.upKey = upKey;
		this.downKey = downKey;
		this.score = 0;
		this.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		scoreText = new JLabel(SCORE_TEXT_START + score + SCORE_TEXT_END);
		this.add(scoreText);
		this.validate();
	}
	
	public Player(Vector2 position, char upKey, char downKey, Color backgroundColor){
		this("Player", position, upKey, downKey, backgroundColor);
	}
	
	public Player(Vector2 position, char upKey, char downKey){
		this(position, upKey, downKey, new Color(0xFFFFFF));
	}
	
	public void keyPressed(KeyEvent e){
				
		if((char)e.getKeyCode() == upKey){
			this.setSpeed(new Vector2(0, -playerSpeed));
		} else if((char)e.getKeyCode() == downKey){
			
			this.setSpeed(new Vector2(0, playerSpeed));
		}
	}
	
	public void keyReleased(KeyEvent e){
		if((char)e.getKeyCode() == upKey || (char)e.getKeyCode() == downKey){
			this.setSpeed(Vector2.ZERO);
		}
	}
	
	public void keyTyped(KeyEvent e){}
	
	public char getUpKey(){ return this.upKey; }
	public char getDownKey(){ return this.downKey; }
	
	public int getScore(){ return this.score; }
	public void resetScore(){ 
		this.score = -1;
		this.incrementScore();
	}
	public void incrementScore(){
		
		this.score++;
		scoreText.setText(SCORE_TEXT_START + score + SCORE_TEXT_END);
		GameController.getGameController().checkPlayerWin(this);
		this.validate();
	}
}
